# Deprecated 17-OCT-2022

This module is only in active use on `home.cern`.

Additionally, the functionality offered by the module is not actually used on `home.cern`.

As such, as of 17-OCT-2022, we are marking this module as deprecated and will not be converting to PHP 8.1 / Drupal 10.

## CERN Twig function

Drupal module that provides a set of Twig functions/filters to be used in templates
and views. Until version 1.1.0, the available filters are:

1. **is_404**
2. **is_image**

## Get Started

In order to get started you just need to place the module under the /modules folder of your website. Then navigate to
the modules interface and install the module.

## Available filters / functions

- **is_404(url: string)**: Gets a url string as input and returns true if the URL exists and false if not.

  **Examples**

  _Function_

  ```twig
     {% if is_404(field_link__uri) %}
         It is 404
     {% else %}
         It is not 404
     {% endif %}
  ```

  _Filter_

  ```twig
  {{ url | is_404 }}
  ```

- **is_image(url: string)**: Gets a url string as input and returns true if the URL is an image and false if not.

  **Examples**

  _Function_

  ```twig
     {% if is_image(field_link__uri) %}
         It is an image
     {% else %}
         It is not an image
     {% endif %}
  ```

  _Filter_

  ```twig
  {{ url | is_image }}
  ```

## Versioning

For the versions available, see the
[tags of this repository](https://gitlab.cern.ch/web-team/drupal/internal/d8/modules/twig-functions/tags).

## License

Like [all Drupal themes and modules](https://www.drupal.org/about/licensing), the
Web Team Packages are licensed under the GPL v3.0 license. See the [LICENSE.md](LICENSE.md)
file for more details.
