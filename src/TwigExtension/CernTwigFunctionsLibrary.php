<?php


namespace Drupal\cern_twig_functions_library\TwigExtension;



class CernTwigFunctionsLibrary extends \Twig_Extension {

	public function getName()
	{
		return 'cern_twig_functions_library.twig_extension';
	}

	/**
	 * {@inheritdoc}
	 * This function must return the name of the extension. It must be unique.
	 */
	/**
	 * {@inheritdoc}
	 */
	public function getFilters() {
		return [
				new \Twig_SimpleFilter('is_404', array($this, 'is404')),
				new \Twig_SimpleFilter('is_image', array($this, 'isImage')),
		];
	}


	/**
	 * In this function we can declare the extension function.
	 */
	public function getFunctions() {
		return array(
				new \Twig_SimpleFunction('is_404', [$this, 'is404']),
				new \Twig_SimpleFunction('is_image', [$this, 'isImage']),
		);
	}


	/**
	 * Checks if a URL returns 404 or not
	 *
	 * @param $url string the URL to be checked
	 * @return bool true if its 404, false if not
	 */
	public function is404($url) {

		$handle = curl_init($url);
		curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

		/* Check for 404 (file not found). */
		$httpCode = curl_getinfo($handle, CURLINFO_RESPONSE_CODE);
		curl_close($handle);
//		return $httpCode;
		if ($httpCode == 0){
			return true;
		}
		else{
			return false;
		}

	}


	/**
	 * Checks if a URL is an image
	 *
	 * @param $url string the URL to be checked
	 * @return bool true if it is an image, false if not
	 */
	public function isImage($url) {
		$headers = get_headers($url, 1);

		if (array_key_exists('Content-Disposition',$headers)) {
			return true;
		} else {
			return false;
		}
	}
}

