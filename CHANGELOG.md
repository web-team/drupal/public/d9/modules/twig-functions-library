#Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.1] - 07/12/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [1.1.0] - 23-04-2019

- Added _is_image_ function/filter

## [1.0.0] - 18-04-2019

- Initialized module
- Added _is_404_ function/filter
